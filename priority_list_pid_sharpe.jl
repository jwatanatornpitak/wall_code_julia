using ArgParse
using CSV
using Dates
using MySQL
using DataFrames
using BenchmarkTools
using DataFramesMeta
using Statistics
using StatsBase
using Distributed
using SharedArrays
using JSON


function parse_commandline()
    s = ArgParseSettings()
    
    @add_arg_table s begin
        "--SubflowID"
            help = "subflowID to update"
            arg_type = Int
            required = true
        "--ProfileID"
            help = "profileID to update"
            arg_type = Int
            required = true
        "--min_payout_1"
            help = "min payouts for layer 1"
            default = 3.5:4.5:5:6.5:7.5:8.5 
        "--min_payout_2"
            help = "min payouts for layer 2"
            default = 3.5:4.5:5 
        "--min_payout_3"
            help = "min payouts for layer 3"
            default = 0.6:1.5:1.6:1.8:2.8:3.5
        "--query"
            help = "SQL query"
            arg_type = String
            required = true
	    "--todays_date"
	        help = "today's date"
	        arg_type = String
            required = true
        "--user"
            help = "user credentials for db"
            arg_type = String    
            required = true
        "--pswd_authentication"
            help = "password credentials for db"
            arg_type = String    
            required = true
        "--host"
            help = "host address for db"
            arg_type = String    
            required = true
        "--dbname"
            help = "name of db"
            arg_type = String    
            required = true
    end
    return parse_args(s)
end


function getStackID(input_selected_subflow_df::DataFrame, input_subflow_id::Int, input_profile_id::Int)
    
    if input_profile_id in input_selected_subflow_df[:ProfileID]
        
        if 1 in input_selected_subflow_df[:Page]
            
            sort!(input_selected_subflow_df, :Date)
            
            stack_id = input_selected_subflow_df[(input_selected_subflow_df[:Page] .== 1) .& 
                                                 (input_selected_subflow_df[:ProfileID] .== input_profile_id) .&
                                                 (map(x -> !ismissing(x), input_selected_subflow_df[:Position])),
                                                 [:Date, :StackID]][end,:StackID] 
            if stack_id .=== missing
                
                println("""No stack id found in the stack id column for inputted profileid:
                    Given inputted_subflow_id: $input_subflow_id and inputted_profile_id: $input_profile_id""")
               
            else
                
                return stack_id
                
            end
           
        else
            
            page_mcid_df = unique(selected_subflow_df[(selected_subflow_df[:Position] .=== missing) .&
                                                      (selected_subflow_df[:ProfileID] .== input_profile_id),
                                                      [:Page, :CampaignID]])
            page_mcid_df[:CampaignID] = map(x -> convert(Int32, x), page_mcid_df[:CampaignID])
            
            println("""ProfileID found but can't find page 1 in the page column. Found these for the inputted profileID:
                $page_mcid_df""")
            
        end
            
    else
        
        println("""Profile id not found. Please make sure the profile id in ML Scheduler is the right one and that there's traffic
                going to that profile id. Given inputted_subflow_id: $input_subflow_id and inputted_profile_id: $input_profile_id""") 
            
    end


end


function joinUnderScoreDelim(x)
        join(x, "_")
end

function cleanVidCidDuplicates(input_page_1_selected_subflow_df::DataFrame)    
    
    input_page_1_selected_subflow_grouped_df = by(input_page_1_selected_subflow_df, :VID_CID) do df
                                                 (Revenue_maximum = maximum(df[:, :Revenue]),
                                                  Priority_minimum = minimum(df[:, :Priority])) end
    
    input_page_1_selected_subflow_df = join(input_page_1_selected_subflow_df, input_page_1_selected_subflow_grouped_df,
                                            kind=:left, on=:VID_CID)
    
    input_page_1_selected_subflow_df = input_page_1_selected_subflow_df[[:VID, :CampaignID,
                                                                         :Priority_minimum, :Revenue_maximum]]
    
    names!(input_page_1_selected_subflow_df, Symbol.(["VID", "CampaignID", "Priority", "Revenue"]))
    
    input_page_1_selected_subflow_df = unique(input_page_1_selected_subflow_df)
    
end

function cleanPage1DF(input_page_1_selected_subflow_df::DataFrame)
    
    input_page_1_selected_subflow_df[:VID_CID] = map((a,b) -> string(a) * "_" * string(b),
                                                              input_page_1_selected_subflow_df[:VID],
                                                              input_page_1_selected_subflow_df[:CampaignID])
    
    input_page_1_selected_subflow_df = cleanVidCidDuplicates(input_page_1_selected_subflow_df)
            
    input_page_1_selected_subflow_df[:Converted] = map(x -> x > 0 ? 1 : 0 , input_page_1_selected_subflow_df[:Revenue])
    
    page_1_vid_cid_seq_table = by(input_page_1_selected_subflow_df, :VID, :CampaignID => joinUnderScoreDelim)
    
    input_page_1_selected_subflow_df = join(input_page_1_selected_subflow_df, page_1_vid_cid_seq_table, kind=:left, on=:VID)   
    
    return input_page_1_selected_subflow_df
    
end


function getCIDSeqTable(input_page_1_selected_subflow_df::DataFrame, input_min_conv::Int)
    
    page_1_cid_seq_keep_table = by(input_page_1_selected_subflow_df, :CampaignID_joinUnderScoreDelim) do df
                              (Conversions = sum(df[:, :Converted]),
                               Rev = sum(df[:, :Revenue]),
                               Users = length(unique(df[:, :VID]))) end
    
    page_1_cid_seq_keep_table = page_1_cid_seq_keep_table[(page_1_cid_seq_keep_table[:Conversions] .>= input_min_conv), :]
    
    if nrow(page_1_cid_seq_keep_table) > 0
        
        page_1_cid_seq_keep_table[:RPU] = map((x,y) -> x/y , page_1_cid_seq_keep_table[:Rev], page_1_cid_seq_keep_table[:Users])
        
        page_1_cid_seq_median_rpu = div(nrow(page_1_cid_seq_keep_table), 2) 
        
        page_1_cid_seq_keep_table = page_1_cid_seq_median_rpu < 10 ? page_1_cid_seq_keep_table[:CampaignID] : 
                                    sort!(page_1_cid_seq_keep_table, :RPU, rev = true)[1:page_1_cid_seq_median_rpu, 
                                    :CampaignID_joinUnderScoreDelim]
        
        return page_1_cid_seq_keep_table
        
    else
        println("No baskets with inputted minimum conversions. Try decreasing the minimum conversion requirement or increase the date range")
        
    end
        
end


function bootstrapSamples(boot_rounds::Int, input_boot_df::DataFrame, boot_sample_size::Int)
    rpu_samples = zeros(boot_rounds)
    for b=1:boot_rounds
        boot_index = sample(1:boot_sample_size, boot_sample_size, replace=true)
        rpu_samples[b] = mean(map(x -> input_boot_df[x,:Revenue_sum] , boot_index))
    end
    return rpu_samples
end


function calcSharpe(boot_rounds::Int64, input_boot_df::DataFrame, boot_sample_size::Int)
    booted = bootstrapSamples(boot_rounds, input_boot_df, boot_sample_size)
    booted_mean, booted_sd = map( (x) -> x(booted), [mean, std])
    return booted_mean, booted_sd
end


function getBasketStats(input_page_1_selected_subflow_df::DataFrame, input_min_conv::Int, boot_rounds::Int)
    
    page_1_cid_seq_keep_table = getCIDSeqTable(input_page_1_selected_subflow_df, input_min_conv)
    
    if page_1_cid_seq_keep_table != nothing
        
        baskets_stats = hcat(DataFrame(CampaignID_joinUnderScoreDelim = page_1_cid_seq_keep_table),
            DataFrame([Float64, Float64, Float64], [:mean_rpu, :sd_rpu, :sharpe], length(page_1_cid_seq_keep_table)))
        
        for (i, cid_seq) in enumerate(page_1_cid_seq_keep_table)
            
            boot_cid_seq_df = by(input_page_1_selected_subflow_df[input_page_1_selected_subflow_df[:CampaignID_joinUnderScoreDelim] .==
                    cid_seq, [:VID, :Revenue]], :VID, :Revenue => sum)
            
            baskets_stats[i,:mean_rpu], baskets_stats[i,:sd_rpu] = calcSharpe(boot_rounds, boot_cid_seq_df, nrow(boot_cid_seq_df))
        end
        
        baskets_stats[:sharpe] = map((x,y) -> (x-mean(baskets_stats[:mean_rpu]))/y, baskets_stats[:mean_rpu], baskets_stats[:sd_rpu])
        
        sort!(baskets_stats, :sharpe, rev = true)
        
    end

end


function getPriorityMat(input_page_1_selected_subflow_df::DataFrame)
    output_priority_mat = by(input_page_1_selected_subflow_df, :Priority, :Priority => length) 
    sort!(output_priority_mat, :Priority)
    output_priority_mat[:Coverage] = output_priority_mat[:Priority_length] / sum(output_priority_mat[:Priority_length])
    output_priority_mat[:Cumulative_Coverage] = cumsum(output_priority_mat[:Coverage])
    output_priority_mat[:Negate_Coverage] = map(x-> 1 - x, output_priority_mat[:Cumulative_Coverage])
    return output_priority_mat
end


function calcBaskets(input_baskets_sharpe)
    second_div = diff(diff(input_baskets_sharpe))
    output = [1, 1, 1, 1]
    switches = findall(second_div .< 0)[1:3]
    switches = switches.+1
    output[2:4] = switches
    unique(output)[1:3]
end


function expandGrid(levels...)
    lengths = length.(levels)
    inner = 1
    outer = prod(lengths)
    grid = []
    for i in 1:length(levels)
        outer = div(outer, lengths[i])
        push!(grid, repeat(levels[i], inner=inner, outer=outer))
        inner *= lengths[i]
    end
    Tuple(grid)
end


function getTuneGrid(input_baskets_sharpe::Array{Float64}, input_min_payout_1_array::Array{Float64},
                     input_min_payout_2_array::Array{Float64}, input_min_payout_3_array::Array{Float64},
                     input_layer_1_adj_factor::Array{Float64}, input_layer_2_adj_factor::Array{Float64},
                     input_layer_3_adj_factor::Array{Float64})
    
    b_baskets = calcBaskets(input_baskets_sharpe)
    
    tune_grid = convert(DataFrame, reduce(hcat, expandGrid(b_baskets, 
                                                           input_min_payout_1_array,
                                                           input_min_payout_2_array,
                                                           input_min_payout_3_array,
                                                           input_layer_1_adj_factor,
                                                           input_layer_2_adj_factor,
                                                           input_layer_3_adj_factor)))
    
    names!(tune_grid, Symbol.(["baskets", "min_payout_1", "min_payout_2", "min_payout_3",
                               "layer_1_adj_factor", "layer_2_adj_factor", "layer_3_adj_factor"]))
    
    tune_grid[:baskets] = map(x -> Int32(x), tune_grid[:baskets])
    tune_grid = tune_grid[(tune_grid[:min_payout_1] .> tune_grid[:min_payout_2]) .& 
                          (tune_grid[:min_payout_2] .> tune_grid[:min_payout_3]),:]
    unique!(tune_grid)
    
end


function replaceNaNCol(input_col)
   map(x -> isnan(x) ? zero(x) : x, input_col) 
end


function getCampaignStatsOverall(input_selected_subflow_df::DataFrame)
    
    campaign_stats_overall = by(input_selected_subflow_df, :CampaignID) do df
        (Conversions = sum(df[:, :Converted]), Rev = sum(df[:, :Revenue]), Users = length(unique(df[:, :VID]))) 
    end
    
    campaign_stats_overall[:RPU] = map((x,y) -> x/y, campaign_stats_overall[:Rev], campaign_stats_overall[:Users])
    campaign_stats_overall[:CR] = map((x,y) -> x/y, campaign_stats_overall[:Conversions], campaign_stats_overall[:Rev])
    campaign_stats_overall[:CR] = replaceNaNCol(campaign_stats_overall[:CR])
    campaign_stats_overall[:RPU] = replaceNaNCol(campaign_stats_overall[:RPU])
    campaign_stats_overall = campaign_stats_overall[[:CampaignID, :RPU, :CR]]
    sort!(campaign_stats_overall, :RPU, rev=true)
    
end

function getTopSeq(input_page_1_df::DataFrame, input_num_baskets::Int32, input_min_payout_1::Float64,
                   input_baskets_stats::DataFrame, input_campaign_payout::DataFrame)
    
    page_1_top_seq = input_page_1_df[in(input_baskets_stats[1:input_num_baskets,1]).(input_page_1_df.CampaignID_joinUnderScoreDelim), :] 
   
    top_seq_stats = by(page_1_top_seq, :CampaignID) do df
    (Conversions = sum(df[:, :Converted]), Rev = sum(df[:, :Revenue]), Users = length(unique(df[:, :VID])),
            Count = length(unique(df[:, :CampaignID_joinUnderScoreDelim]))) end
    
    top_seq_stats = join(top_seq_stats, unique(input_campaign_payout[[:CampaignID, :Payout]]), kind=:left, on=:CampaignID)
    
    top_seq_stats = top_seq_stats[top_seq_stats[:Payout] .> input_min_payout_1, :]
    
    top_seq_stats[:RPU] = map((x,y) -> x/y, top_seq_stats[:Rev], top_seq_stats[:Users])
    top_seq_stats[:CR] = map((x,y) -> x/y, top_seq_stats[:Conversions], top_seq_stats[:Users])
    
    top_seq_stats[:CR] = replaceNaNCol(top_seq_stats[:CR])
    top_seq_stats[:RPU] = replaceNaNCol(top_seq_stats[:RPU])
    
    top_seq_stats = top_seq_stats[top_seq_stats[:RPU] .> percentile(top_seq_stats[:RPU], 25),:]
    
    sort!(top_seq_stats, [:Count, :RPU], rev=true)
    
    top_seq_stats[[:CampaignID, :RPU, :CR, :Payout]]
end


function filterTopSeqStatsOverall(input_campaign_stats_overall::DataFrame, input_top_seq_df::DataFrame)
    input_campaign_stats_overall[.! in(input_top_seq_df[:CampaignID]).(input_campaign_stats_overall[:CampaignID]), :]
end


function checkPayout(input_payout_col::Array, input_min_payout::Float64)
    map(x -> x .> input_min_payout ? 1 : 0, input_payout_col)
end


function adjRPU(input_RPU::Float64, input_adj_factor::Float64)
    input_RPU*input_adj_factor 
end

function moveLowPayoutDownList(input_cat_priority_list::DataFrame)
    
    local_priority_list = input_cat_priority_list
    
    if nrow(local_priority_list) > 20
        
        temp_top_priority_list = local_priority_list[local_priority_list[:Payout] .>= 1, :]
        
        temp_bottom_priority_list = vcat(deepcopy(temp_top_priority_list[20:nrow(temp_top_priority_list), :]),
                                         deepcopy(temp_top_priority_list[temp_top_priority_list[:Payout] .< 1, :]))
        
        sort!(temp_bottom_priority_list, :RPU, rev=true)
        
        output_priority_list = vcat(temp_top_priority_list[1:20,:], temp_bottom_priority_list)
        
    else
        
        output_priority_list = vcat(local_priority_list[local_priority_list[:Payout] .>= 1, :],
                                    local_priority_list[local_priority_list[:Payout] .< 1, :])
    
    end
    
    return output_priority_list

end

function split_layer_1(input_campaign_stats_overall::DataFrame, input_min_payout_1::Float64,
                       input_layer_1_adj_factor::Float64, input_count_top_seq::Int64)
    
    input_campaign_stats_overall[:RPU_adj] = input_campaign_stats_overall[:RPU]
    input_campaign_stats_overall[:High_cap] = checkPayout(input_campaign_stats_overall[:Payout], input_min_payout_1)
        
    adj_factor = 1.5
    
    while (input_count_top_seq <= 15) & (adj_factor < input_layer_1_adj_factor)
        
        adj_factor += 0.1
        
        input_campaign_stats_overall[:RPU_adj] = map((x,y) -> y .== 1 ? adjRPU(x, adj_factor) : x,
                                                input_campaign_stats_overall[:RPU], input_campaign_stats_overall[:High_cap])
        
        sort!(input_campaign_stats_overall, :RPU_adj, rev=true)
        
        input_count_top_seq += findfirst(input_campaign_stats_overall[:High_cap] .== 0) - 1
        
    end
    
    index = findfirst(input_campaign_stats_overall[:High_cap] .== 0)
    input_campaign_stats_overall = input_campaign_stats_overall[[:CampaignID, :RPU, :CR, :Payout]]
    ouput_gradient_layer_1 = input_campaign_stats_overall[1:(index-1),:]
    output_gradient_layer_2 = input_campaign_stats_overall[index:nrow(input_campaign_stats_overall),:]
    sort!(output_gradient_layer_2, :RPU, rev=true)
    
    return ouput_gradient_layer_1, output_gradient_layer_2
    
end


function split_layer_2(input_gradient_layer_2::DataFrame, input_min_payout_2::Float64,
                       input_layer_2_adj_factor::Float64, input_count::Int64)
    
    input_gradient_layer_2[:RPU_adj] = input_gradient_layer_2[:RPU]
    input_gradient_layer_2[:High_cap] = checkPayout(input_gradient_layer_2[:Payout], input_min_payout_2)
        
    adj_factor = 1.5
    
    while (input_count <= 25) & (adj_factor < input_layer_2_adj_factor)
        
        adj_factor += 0.1
        
        input_gradient_layer_2[:RPU_adj] = map((x,y) -> y .== 1 ? adjRPU(x, adj_factor) : x,
                                                input_gradient_layer_2[:RPU], input_gradient_layer_2[:High_cap])
        
        sort!(input_gradient_layer_2, :RPU_adj, rev=true)
        
        input_count += findfirst(input_gradient_layer_2[:High_cap] .== 0) - 1
        
    end
    
    index = findfirst(input_gradient_layer_2[:High_cap] .== 0)
    input_gradient_layer_2 = input_gradient_layer_2[[:CampaignID, :RPU, :CR, :Payout]]
    output_gradient_layer_2 = input_gradient_layer_2[1:(index-1),:]
    output_gradient_layer_3 = input_gradient_layer_2[index:nrow(input_gradient_layer_2),:]
    sort!(output_gradient_layer_3, :RPU, rev=true)
    
    return output_gradient_layer_2, output_gradient_layer_3
    
end


function split_layer_3(input_gradient_layer_3::DataFrame, input_min_payout_3::Float64,
                       input_layer_3_adj_factor::Float64, input_count::Int64)
    
    input_gradient_layer_3[:RPU_adj] = input_gradient_layer_3[:RPU]
    input_gradient_layer_3[:High_cap] = checkPayout(input_gradient_layer_3[:Payout], input_min_payout_3)
        
    adj_factor = 1.5
    
    while (input_count <= 40) & (adj_factor < input_layer_3_adj_factor)
        
        adj_factor += 0.1
        
        input_gradient_layer_3[:RPU_adj] = map((x,y) -> y .== 1 ? adjRPU(x, adj_factor) : x,
                                                input_gradient_layer_3[:RPU], input_gradient_layer_3[:High_cap])
        
        sort!(input_gradient_layer_3, :RPU_adj, rev=true)
        
        input_count += findfirst(input_gradient_layer_3[:High_cap] .== 0) - 1
        
    end
    
    index = findfirst(input_gradient_layer_3[:High_cap] .== 0)
    input_gradient_layer_3 = input_gradient_layer_3[[:CampaignID, :RPU, :CR, :Payout]]
    output_gradient_layer_3 = input_gradient_layer_3[1:(index-1),:]
    output_gradient_layer_4 = input_gradient_layer_3[index:nrow(input_gradient_layer_3),:]
    sort!(output_gradient_layer_4, :RPU, rev=true)
    
    return output_gradient_layer_3, output_gradient_layer_4
    
end


function getPriority(input_page_1_df::DataFrame, input_campaign_payout::DataFrame,
                     input_campaign_stats_overall::DataFrame, input_baskets_stats::DataFrame,
                     input_num_baskets::Int32, input_min_payout_1::Float64,
                     input_min_payout_2::Float64, input_min_payout_3::Float64,
                     input_layer_1_adj_factor::Float64, input_layer_2_adj_factor::Float64,
                     input_layer_3_adj_factor::Float64)
    
    top_seq = getTopSeq(input_page_1_df, input_num_baskets, input_min_payout_1, input_baskets_stats, input_campaign_payout)
    
    campaign_stats_overall_filtered = filterTopSeqStatsOverall(input_campaign_stats_overall, top_seq)
    
    campaign_stats_overall_filtered = join(campaign_stats_overall_filtered, input_campaign_payout,
                                           kind=:left, on=:CampaignID)
    campaign_stats_overall_filtered[:RPU] = map(x -> Float64(x), campaign_stats_overall_filtered[:RPU])
    
    count_top_seq = nrow(top_seq)

    gradient_layer_1, gradient_layer_2 = split_layer_1(campaign_stats_overall_filtered, input_min_payout_1,
                                                      input_layer_1_adj_factor, count_top_seq)
    
    count_top_layer_1 = count_top_seq + nrow(gradient_layer_1)

    gradient_layer_2, gradient_layer_3 = split_layer_2(deepcopy(gradient_layer_2), input_min_payout_2,
                                                       input_layer_2_adj_factor, count_top_layer_1)
    
    count_top_layer_1_2 = count_top_seq + nrow(gradient_layer_1) + nrow(gradient_layer_2)

    gradient_layer_3, gradient_layer_4 = split_layer_3(deepcopy(gradient_layer_3), input_min_payout_3,
                                                       input_layer_3_adj_factor, count_top_layer_1_2)
    
    cat_priority_list = reduce(vcat, [top_seq, gradient_layer_1, gradient_layer_2, gradient_layer_3, gradient_layer_4])
    
    moveLowPayoutDownList(cat_priority_list)
 
end

function getPriorityTuneGrid(input_page_1_df::DataFrame, input_campaign_payout::DataFrame,
                             input_campaign_stats_overall::DataFrame, input_baskets_stats::DataFrame,
                             input_priority_cutoff::Int64, input_tune_grid::DataFrame)
    
   map((a, b, c, d, e, f, g) -> getPriority(input_page_1_df, input_campaign_payout, input_campaign_stats_overall,
                                  input_baskets_stats, a, b, c, d, e, f, g)[1:input_priority_cutoff,:CampaignID],
                    input_tune_grid[:baskets], input_tune_grid[:min_payout_1],
                    input_tune_grid[:min_payout_2], input_tune_grid[:min_payout_3],
                    input_tune_grid[:layer_1_adj_factor], input_tune_grid[:layer_2_adj_factor],
                    input_tune_grid[:layer_3_adj_factor])
    
end
        
function checkOverlapPriorityList(input_tune_grid::DataFrame, input_tune_grid_priority_check::Array)
    map(x -> mean(input_tune_grid_priority_check .== x), input_tune_grid[:priority_list])
end


function discardOverlapPriorityList(input_tune_grid::DataFrame, input_overlap_cutoff::Float64)
    
    index_to_del = Set{Int64}()
    
    for i in 1:nrow(input_tune_grid)
    
        if i in index_to_del
            continue
        else
            index_to_del_to_add = checkOverlapPriorityList(input_tune_grid,
                                                            input_tune_grid[i,:priority_list]) .>=
                                                            input_overlap_cutoff
            index_to_del_to_add[i] = false
            union!(index_to_del, findall(index_to_del_to_add))
        end
    
    end
    
    input_tune_grid[setdiff(1:nrow(input_tune_grid), index_to_del),:]
    
end
        
function getBacktestGrid(input_baskets_stats::DataFrame, input_min_payout_1_array::Array{Float64},
                         input_min_payout_2_array::Array{Float64}, input_min_payout_3_array::Array{Float64},
                         input_layer_1_adj_factor::Array{Float64}, input_layer_2_adj_factor::Array{Float64},
                         input_layer_3_adj_factor::Array{Float64},
                         input_selected_subflow_df::DataFrame, input_priority_mat::DataFrame,
                         input_page_1_selected_subflow_df::DataFrame, input_campaign_payout::DataFrame,
                         input_priority_cutoff::Float64, input_overlap_cutoff::Float64)
    
    tune_grid = getTuneGrid(input_baskets_stats[:sharpe], input_min_payout_1_array,
                            input_min_payout_2_array, input_min_payout_3_array, 
                            input_layer_1_adj_factor, input_layer_2_adj_factor,
                            input_layer_3_adj_factor)
    
    campaign_stats_overall = getCampaignStatsOverall(input_selected_subflow_df)
        
    priority_cutoff = findfirst(input_priority_mat[:Cumulative_Coverage] .> input_priority_cutoff)
    
    priority_cutoff = priority_cutoff < 10 ? 10 : priority_cutoff
    
    println("input_priority_cutoff: $input_priority_cutoff -> priority position: $priority_cutoff")

    tune_grid[:priority_list] = getPriorityTuneGrid(input_page_1_selected_subflow_df, input_campaign_payout,
                                                    campaign_stats_overall, input_baskets_stats, priority_cutoff, tune_grid)
    
    discardOverlapPriorityList(tune_grid, input_overlap_cutoff)
        
end


function getBacktestIndex(input_page_1_selected_subflow_df_cid_seq_col::Array, input_cid::String)
    map(x -> occursin(input_cid, x), input_page_1_selected_subflow_df_cid_seq_col)
end


function getSetBacktestIndex(input_page_1_selected_subflow_df_cid_seq_col::Array, input_cid_vec::Array)
    
    input_cid_vec = map(x-> string(x), input_cid_vec)
    
    index_to_count = reduce(vcat, map(x -> findall(getBacktestIndex(input_page_1_selected_subflow_df_cid_seq_col,x)), input_cid_vec))
    
    count_index_to_backtest = countmap(index_to_count)
        
    index_to_backtest = [k for (k,v) in count_index_to_backtest if v > 3]
    
end

function getWeigthedSharpe(input_page_1_selected_subflow_df::DataFrame, input_cids_to_boot::Array, input_priority_weights::Array)
    
    n = length(input_cids_to_boot) - 4
    
    sharpe_vec = zeros(n)
    
    for i in 1:n
        
        cid_vec = input_cids_to_boot[1:(4+i)]
                    
        set_to_backtest = getSetBacktestIndex(input_page_1_selected_subflow_df[:CampaignID_joinUnderScoreDelim], cid_vec)
        
        if !isempty(set_to_backtest) 
            
            backtest_boot_df = by(input_page_1_selected_subflow_df[intersect(1:nrow(input_page_1_selected_subflow_df),
                                    set_to_backtest), :], :VID, :Revenue => sum)
            
            pl_df = backtest_boot_df[:Revenue_sum]
            pl_df = map(x->Float64(x), pl_df)
            pl_df = convert(SharedArray, pl_df)
            
            backtest_boot_size = nrow(backtest_boot_df)
            
            b_rounds = 50
            
            rpu_booted = reduce(vcat, pmap(x -> bootstrapSamplesPL(x, pl_df, backtest_boot_size),
                                [b_rounds, b_rounds, b_rounds, b_rounds, b_rounds, b_rounds, b_rounds, b_rounds]))
            
            booted_mean, booted_sd = map( (x) -> x(rpu_booted), [mean, std])
            
            sharpe_vec[i] = booted_mean/ booted_sd
        
        end
    
    end
    
    sum(input_priority_weights[1:n] .* sharpe_vec) 

end
            
            
            
function getBacktestParamResults(input_backtest_grid::DataFrame, input_n_folds::Int64,
                                 input_page_1_selected_subflow_df::DataFrame, input_priority_mat::DataFrame)
    
    backtest_results = zeros(nrow(input_backtest_grid), input_n_folds)
    
    index_to_del = Set{Int64}()
    
    for i in 1:input_n_folds
        
        index_to_pop = setdiff(1:nrow(input_backtest_grid), index_to_del)
                    
        if length(index_to_pop) > 1
                        
            backtest_results[index_to_pop,i] = map(x -> getWeigthedSharpe(input_page_1_selected_subflow_df, x,
                                                                          input_priority_mat[:Negate_Coverage]),
                                                                          input_backtest_grid[index_to_pop,:priority_list])
                    
            backtest_results[:,i] = map(x -> isnan(x) ? zero(x) : x, backtest_results[:,i])
                            
            union!(index_to_del, findall(backtest_results[:,i] .< (maximum(backtest_results) - 10)))
                        
        end
                    
        println(backtest_results[index_to_pop,:])
    
    end
    
    backtest_results_rowmean = [mean(backtest_results[row, :]) for row=1:size(backtest_results)[1]] 
    
    input_backtest_grid[findfirst(backtest_results_rowmean .== maximum(backtest_results_rowmean)),
                       [:baskets, :min_payout_1, :min_payout_2, :min_payout_3, 
                        :layer_1_adj_factor, :layer_2_adj_factor, :layer_3_adj_factor]]

end
            
            
function getPriorityFinal(input_page_1_df::DataFrame, input_campaign_payout::DataFrame,
                     input_campaign_stats_overall::DataFrame, input_baskets_stats::DataFrame,
                     input_num_baskets::Int32, input_min_payout_1::Float64,
                     input_min_payout_2::Float64, input_min_payout_3::Float64,
                     input_layer_1_adj_factor::Float64, input_layer_2_adj_factor::Float64,
                     input_layer_3_adj_factor::Float64)
    
    top_seq = getTopSeq(input_page_1_df, input_num_baskets, input_min_payout_1, input_baskets_stats, input_campaign_payout)
    
    campaign_stats_overall_filtered = filterTopSeqStatsOverall(input_campaign_stats_overall, top_seq)
    
    campaign_stats_overall_filtered = join(campaign_stats_overall_filtered, input_campaign_payout,
                                           kind=:left, on=:CampaignID)
    campaign_stats_overall_filtered[:RPU] = map(x -> Float64(x), campaign_stats_overall_filtered[:RPU])
    
    count_top_seq = nrow(top_seq)

    gradient_layer_1, gradient_layer_2 = split_layer_1(campaign_stats_overall_filtered, input_min_payout_1,
                                                      input_layer_1_adj_factor, count_top_seq)
    
    count_top_layer_1 = count_top_seq + nrow(gradient_layer_1)

    gradient_layer_2, gradient_layer_3 = split_layer_2(deepcopy(gradient_layer_2), input_min_payout_2,
                                                       input_layer_2_adj_factor, count_top_layer_1)
    
    count_top_layer_1_2 = count_top_seq + nrow(gradient_layer_1) + nrow(gradient_layer_2)

    gradient_layer_3, gradient_layer_4 = split_layer_3(deepcopy(gradient_layer_3), input_min_payout_3,
                                                       input_layer_3_adj_factor, count_top_layer_1_2)
    
    cat_priority_list = reduce(vcat, [top_seq, gradient_layer_1, gradient_layer_2, gradient_layer_3, gradient_layer_4])
    
    cat_priority_list = moveLowPayoutDownList(cat_priority_list)
    
    return cat_priority_list, top_seq, gradient_layer_1, gradient_layer_2, gradient_layer_3, gradient_layer_4
 
end
            
            
function prioritylist2JSON(input_priority_list_dataframe::DataFrame)
    
    nrow_list = nrow(input_priority_list_dataframe)
    indices_list = names(input_priority_list_dataframe)
    
    priority_list_final_dict = [Dict([string(index) => input_priority_list_dataframe[index][i] for index in indices_list]) for
                                      i in 1:nrow_list]
    JSON.json(priority_list_final_dict)
    
                end;
    

parsed_args = parse_commandline()

conn = MySQL.connect(parsed_args["host"], parsed_args["user"], parsed_args["pswd_authentication"], db = parsed_args["dbname"]);

selected_subflow_df = MySQL.query(conn, query) |> DataFrame;

MySQL.disconnect(conn)

#describe(selected_subflow_df)

if parsed_args["SubflowID"] .== 898929 
    selected_subflow_df[selected_subflow_df[:Page] .== 2, :Page] = 1
    selected_subflow_df[selected_subflow_df[:Page] .== 3, :Page] = 2
    selected_subflow_df[selected_subflow_df[:Page] .== 4, :Page] = 3
end

stack_id = getStackID(selected_subflow_df, parsed_args["SubflowID"], parsed_args["ProfileID"])

selected_subflow_df = selected_subflow_df[(map(x -> !ismissing(x), selected_subflow_df[:Position])), :]

selected_subflow_df = selected_subflow_df[(map(x -> !ismissing(x), selected_subflow_df[:Priority])), :]
    
selected_subflow_df = selected_subflow_df[in(["Engaged Install", "Engaged Drive to Site"]).(selected_subflow_df.CampaignType), :]

selected_subflow_df[:Payout] = map(x -> convert(Float16, x), selected_subflow_df[:Payout])
selected_subflow_df[:Revenue] = map(x -> convert(Float16, x), selected_subflow_df[:Revenue])
campaign_payout = unique(selected_subflow_df[[:CampaignID, :Payout]])

#selected_subflow_df[:CPAStyleID] = map(x -> convert(Int32, x), selected_subflow_df[:CPAStyleID])
#selected_subflow_df[:CPAOfferID] = map(x -> convert(Int32, x), selected_subflow_df[:CPAOfferID])
#campaign_style = unique(selected_subflow_df[:, [:CampaignID, :CPAOfferID, :CPAStyleID]])

selected_subflow_df[:Converted] = map(x -> x > 0 ? 1 : 0 , selected_subflow_df[:Revenue])

selected_subflow_df[:Priority] = map(x -> convert(Int, x), selected_subflow_df[:Priority])
selected_subflow_df[:CampaignID] = map(x -> convert(Int, x), selected_subflow_df[:CampaignID])


page_1_selected_subflow_df = unique(selected_subflow_df[(selected_subflow_df[:Page] .== 1), 
                                                       [:VID, :CampaignID, :Priority, :Revenue]])

page_1_selected_subflow_df = cleanPage1DF(page_1_selected_subflow_df)

baskets_stats = getBasketStats(page_1_selected_subflow_df, 10, 3500)
#if baskets_stats != nothing end
        
min_payout_1 = map(x-> parse(Float64, x), split(parsed_args["min_payout_1"], ":"))
min_payout_2 = map(x-> parse(Float64, x), split(parsed_args["min_payout_2"], ":"))
min_payout_3 = map(x-> parse(Float64, x), split(parsed_args["min_payout_3"], ":"))


priority_mat = getPriorityMat(page_1_selected_subflow_df)
            
layer_1_adj_factor = [2.7, 3, 3.5]
layer_2_adj_factor = [2.7, 3, 3.5, 3.8, 4]
layer_3_adj_factor = [2.7, 3, 3.5, 3.8, 4, 4.2]
            
tune_grid = getTuneGrid(baskets_stats[:sharpe], min_payout_1, min_payout_2, min_payout_3,
                        layer_1_adj_factor, layer_2_adj_factor, layer_3_adj_factor)
    
campaign_stats_overall = getCampaignStatsOverall(selected_subflow_df)

backtest_grid = getBacktestGrid(baskets_stats, min_payout_1, min_payout_2, min_payout_3,
                                layer_1_adj_factor, layer_2_adj_factor, layer_3_adj_factor,
                                selected_subflow_df, priority_mat, page_1_selected_subflow_df,
                                campaign_payout, 0.9, 0.85)
            
rmprocs(workers()); addprocs(8); nworkers()
          
@everywhere using Statistics
@everywhere using StatsBase
@everywhere using SharedArrays

@everywhere function bootstrapSamplesPL(B::Int64, input_rev_array, boot_sample_size::Int64)
    rpu_samples = zeros(B)
    for b=1:B
        boot_index = sample(1:boot_sample_size, boot_sample_size, replace=true)
        rpu_samples[b] = mean(map(x ->  input_rev_array[x], boot_index))
    end
    
    return rpu_samples
end
            

backtest_param_results = getBacktestParamResults(backtest_grid, 4, page_1_selected_subflow_df, priority_mat)

priority_list_final, top_seq, gradient_layer_1, gradient_layer_2, gradient_layer_3, gradient_layer_4 = 
getPriorityFinal(page_1_selected_subflow_df, campaign_payout, campaign_stats_overall, baskets_stats,
                                  backtest_param_results[1,:baskets], backtest_param_results[1,:min_payout_1],
                                  backtest_param_results[1,:min_payout_2], backtest_param_results[1,:min_payout_3],
                                  backtest_param_results[1,:layer_1_adj_factor], backtest_param_results[1,:layer_2_adj_factor],
                                  backtest_param_results[1,:layer_3_adj_factor])

subflowid = parsed_args["SubflowID"]
todays_date = subflowid = parsed_args["todays_date"]

table_name = "$todays_date_$subflowid.csv"

output_json_1 = JSON.json([Dict("name" => table_name, "stackID" => stack_id, "SubflowID" => 914909, 
                                "list" => JSON.parse(prioritylist2JSON(priority_list_final)))])

JSON.print(JSON.parse(output_json_1), 4)
